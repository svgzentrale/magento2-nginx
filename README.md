# nginx for magento2
`1.13`
companion image to https://cloud.docker.com/u/svgdev/repository/docker/svgdev/magento2-php-base

# Features
- socket connection to php-fpm ready at `/sock`
- self signed cert for ssl ready

![Docker Cloud Build Status](https://img.shields.io/docker/cloud/build/svgdev/magento2-nginx?style=for-the-badge)